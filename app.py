import serial
import time
import threading
from aiohttp import web

distance = b'0.00'
ser = None

async def handle_poll(request):
    global distance
    text = "OK"
    try:
        text = "distance " + str(distance.decode()) + "\n"
        # print(text)
    except:
        pass
    return web.Response(text=text)

def update_distance():
    global distance
    while True:
        try:
            line = ser.readline().rstrip(b'\r\n')
            if len(line) > 0:
                # print(line)
                distance = line
        except Exception as e:
            print(e);
            ser.close()
            return
        

if __name__ == '__main__':
    ser = serial.Serial('COM4', timeout=None)
    threadobj = threading.Thread(target=update_distance)
    threadobj.setDaemon(True)
    threadobj.start()

    app = web.Application()
    app.router.add_get('/poll', handle_poll)
    web.run_app(app, host='127.0.0.1', port=12345)

